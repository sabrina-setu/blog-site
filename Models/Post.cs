﻿using System;


namespace BlogSiteDemo.Models
{
	public class Post
	{
		public int ID { get; set; }

		public string Title { get; set; } = "";
		public string Body { get; set; } = "";
		public string Image { get; set; } = "";

		public string Description { get; set; } = "";
		public string Tag { get; set; } = "";
		public string Category { get; set; } = "";

		public DateTime Created { get; set; } = DateTime.Now;
	}
}
