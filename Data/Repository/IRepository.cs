﻿using BlogSiteDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogSiteDemo.Data.Repository
{
	public interface IRepository
	{
		Post GetPost(int id);
		List<Post> GetAllPost();
		List<Post> GetAllPost(string category);
		void AddPost(Post post);
		void UpdatePost(Post post);
		void RemovePost(int id);

		Task<bool> SaveChangesAsync();


	}
}
