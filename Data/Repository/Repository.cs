﻿using BlogSiteDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogSiteDemo.Data.Repository
{
	public class Repository : IRepository
	{
		private AppDbContext _ctext;
		public Repository(AppDbContext ctext)
		{
			_ctext = ctext;
		}
		public void AddPost(Post post)
		{
			_ctext.Posts.Add(post);
		}

		public List<Post> GetAllPost()
		{
			return _ctext.Posts.ToList();
		}
		public List<Post> GetAllPost(string category)
		{
			Func<Post, bool> InCategory = (post) => { return post.Category.ToLower().Equals(category.ToLower()); };
			return _ctext.Posts.Where(post=> InCategory(post)).ToList();
		}

		public Post GetPost(int id)
		{
			return _ctext.Posts.FirstOrDefault(p => p.ID == id);
		}

		public void RemovePost(int id)
		{
			_ctext.Posts.Remove(GetPost(id));
		}

		public void UpdatePost(Post post)
		{
			_ctext.Posts.Update(post);
		}

		public async Task<bool> SaveChangesAsync()
		{
			if (await _ctext.SaveChangesAsync() > 0)
			{
				return true;
			}
			return false;
		}
	}
}
