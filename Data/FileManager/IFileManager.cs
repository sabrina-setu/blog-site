﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace BlogSiteDemo.Data.FileManager
{
	public interface IFileManager
	{
		FileStream Imagestream(string image);
		Task<string> SaveImage(IFormFile image);
		bool RemoveImage(string image);
	}
}
