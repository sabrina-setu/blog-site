using BlogSiteDemo.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogSiteDemo
{
	public class Program
	{
		public static void Main(string[] args)
		{
			var host = CreateHostBuilder(args).Build();

			try
			{
				var scope = host.Services.CreateScope();

				var contx = scope.ServiceProvider.GetRequiredService<AppDbContext>();
				var userMagr = scope.ServiceProvider.GetRequiredService<UserManager<IdentityUser>>();
				var roleMagr = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

				contx.Database.EnsureCreated();

				var adminRole = new IdentityRole("Admin");
				if (!contx.Roles.Any())
				{
					//create a role
					roleMagr.CreateAsync(adminRole).GetAwaiter().GetResult();

				}
				if (!contx.Users.Any(u => u.UserName == "admin"))
				{
					//create an admin
					var adminUser = new IdentityUser
					{
						UserName = "admin",
						Email = "admin@abc.com",


					};
					var result = userMagr.CreateAsync(adminUser, "password").GetAwaiter().GetResult();
					//add role to user
					userMagr.AddToRoleAsync(adminUser, adminRole.Name).GetAwaiter().GetResult();
				}
			}
			catch(Exception e)
			{
				Console.WriteLine(e.Message);
			}

			host.Run();
		}

		public static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args)
				.ConfigureWebHostDefaults(webBuilder =>
				{
					webBuilder.UseStartup<Startup>();
				});
	}
}
