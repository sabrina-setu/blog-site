﻿using BlogSiteDemo.ViewModel;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogSiteDemo.Controllers
{
	public class AuthenticationController : Controller
	{
		private SignInManager<IdentityUser> _signInManager;
		public AuthenticationController(SignInManager<IdentityUser> signInManager)
		{
			_signInManager = signInManager;
		}

		[HttpGet]
		public IActionResult LogIn()
		{
			return View(new LogInViewModel());
		}

		[HttpPost]
		public async Task<IActionResult> LogIn(LogInViewModel logInVM)
		{
			var result = await _signInManager.PasswordSignInAsync(logInVM.UserName, logInVM.Password, false, false);
			return RedirectToAction("Index","Panel");
		}

		[HttpGet]
		public async Task<IActionResult> LogOut()
		{
			await _signInManager.SignOutAsync();
			return RedirectToAction("Index", "Home");
		}
	}
}
