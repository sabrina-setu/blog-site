﻿using BlogSiteDemo.Data.FileManager;
using BlogSiteDemo.Data.Repository;
using BlogSiteDemo.Models;
using BlogSiteDemo.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BlogSiteDemo.Controllers
{
	[Authorize(Roles ="Admin")]
	public class PanelController : Controller
	{
		private IRepository _repo;
		private IFileManager _fileManager;
		public PanelController(IRepository repo, IFileManager fileManager)
		{
			_repo = repo;
			_fileManager = fileManager;
		}
		public IActionResult Index()
		{
			var posts = _repo.GetAllPost();
			return View(posts);
		}

		[HttpGet]
		public IActionResult Edit(int? id)
		{
			if (id == null)
				return View(new PostViewModel());
			else
			{
				var post = _repo.GetPost((int)id);
				return View(new PostViewModel {
				ID = post.ID,
				Title = post.Title,
				Body = post.Body,
				CurrentImage = post.Image,
				Description = post.Description,
				Tag = post.Tag,
				Category = post.Category
				});
			}
		}
		[HttpPost]
		public async Task<IActionResult> Edit(PostViewModel pvm)
		{
			var post = new Post
			{
				ID = pvm.ID,
				Title = pvm.Title,
				Body = pvm.Body,
				Description = pvm.Description,
				Tag = pvm.Tag,
				Category = pvm.Category

			};
			if(pvm.Image==null)
			{
				post.Image = pvm.CurrentImage;
			}
			else
			{
				if (!string.IsNullOrEmpty(pvm.CurrentImage))
					_fileManager.RemoveImage(pvm.CurrentImage);
				post.Image = await _fileManager.SaveImage(pvm.Image);
			}
			if (post.ID > 0)
			{
				_repo.UpdatePost(post);
			}
			else
				_repo.AddPost(post);
			if (await _repo.SaveChangesAsync())
				return RedirectToAction("Index");
			else
				return View(post);
		}
		[HttpGet]
		public async Task<IActionResult> Remove(int id)
		{
			_repo.RemovePost(id);
			await _repo.SaveChangesAsync();
			return RedirectToAction("Index");


		}
	}
}
