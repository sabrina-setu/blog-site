﻿using BlogSiteDemo.Data;
using BlogSiteDemo.Data.FileManager;
using BlogSiteDemo.Data.Repository;
using BlogSiteDemo.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace BlogSiteDemo.Controllers
{
	public class HomeController : Controller
	{
		private IRepository _repo;
		private IFileManager _fileManager;

		public HomeController(IRepository repo,IFileManager fileManager)
		{
			_repo = repo;
			_fileManager = fileManager;
		}

		//private readonly ILogger<HomeController> _logger;

		//public HomeController(ILogger<HomeController> logger)
		//{
		//	_logger = logger;
		//}

		public IActionResult Index(string category)
		{
			var posts =string.IsNullOrEmpty(category)?_repo.GetAllPost():_repo.GetAllPost(category);
			return View(posts);
		}

		public IActionResult Post(int id)
		{
			var post = _repo.GetPost(id);
			return View(post);
		}

		[HttpGet("/Image/{image}")]
		public IActionResult Image(string image)
		{
			var mime= image.Substring(image.LastIndexOf('.')+1);
			return new FileStreamResult(_fileManager.Imagestream(image),$"image/{mime}");
		}

		//public IActionResult Privacy()
		//{
		//	return View();
		//}

		//[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		//public IActionResult Error()
		//{
		//	return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		//}
	}
}
